import React, {useState, useMemo, useEffect, useReducer} from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import Ionicons from 'react-native-vector-icons/dist/Ionicons';

import Splash from '../screens/Splash';
import Home from "../screens/Home";
import Search from "../screens/Search";
import Detail from "../screens/Detail";

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

export const NewsStack = () => (
    <Stack.Navigator headerMode="none" >
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Detail" component={Detail} />
    </Stack.Navigator>
)

export const MainTabScreen = () => (
    <Tab.Navigator tabBarOptions={{
        activeTintColor: '#003366',
      }}>
        <Tab.Screen name="Home" component={NewsStack} options={{
            tabBarLabel: 'Home',
            tabBarIcon: ({ color, size }) => (
                <Ionicons name="home" color={color} size={size} />
            ),
            }} />
        <Tab.Screen name="Search" component={Search}  options={{
            tabBarLabel: 'Search',
            tabBarIcon: ({ color, size }) => (
                <Ionicons name="search" color={color} size={size} />
            ),
            }} />
    </Tab.Navigator>
)


export const RootStack = ({initialRouteName}) => (
    <Stack.Navigator headerMode="none" initialRouteName={initialRouteName}>
        <Stack.Screen name="MainScreen" component={MainTabScreen} />
    </Stack.Navigator>
)

const Navigator = () => {
    const [isLoadingMainScreen, setisLoadingMainScreen] = useState(true);
    
    useEffect(()=>{
        setTimeout(()=>{
            setisLoadingMainScreen(false)
        }, 2000)
    }, [])

    if(isLoadingMainScreen) {
        return <Splash />
    }


    return (
        <NavigationContainer>
            <RootStack initialRouteName="MainScreen" />
        </NavigationContainer>
    )
}
export default Navigator;
