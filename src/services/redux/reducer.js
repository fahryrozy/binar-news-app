const initialState = {
    auth: {},
    token: null,
    isAdvancedSearch: false,
    isSetStartDate: false,
    isSetEndDate: false,
    selectedStartDate: null,
    selectedEndDate: null,
    searchQueries: {},
}

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ADVANCED_SEARCH':
            console.log('Advance Search')
            return {
                ...state,
                isAdvancedSearch: true,
                searchQueries: action.payload,

            }
        
        case 'QUERY_SEARCH':
            console.log('Query Search')
            return {
                ...state,
                isAdvancedSearch: false,
                searchQueries: {},
            }
        case 'SET_START_DATE':
            console.log('Start Date')
            return {
                ...state,
                isSetStartDate: true,
                selectedStartDate: action.payload,
            }
        
        case 'SET_END_DATE':
            console.log('Set End Date')
            return {
                ...state,
                isSetEndDate: true,
                selectedEndDate: action.payload,
            }
        case 'REMOVE_START_DATE':
            console.log('Remove End Date')
            return {
                ...state,
                isSetStartDate: false,
                selectedStartDate: null,
            }
        
        case 'REMOVE_END_DATE':
            console.log('Remove End Date')
            return {
                ...state,
                isSetEndDate: false,
                selectedEndDate: null,
            }
        default:
            return state
    }
}