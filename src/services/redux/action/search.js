export const adv_search = (queries) => ({
    type: 'ADVANCED_SEARCH',
    payload: queries,
});

export const query_search = () => ({
    type: 'QUERY_SEARCH',
});

export const set_startDate = (date) => ({
    type: 'SET_START_DATE',
    payload: date,
})

export const set_endDate = (date) => ({
    type: 'SET_END_DATE',
    payload: date,
})

export const remove_startDate = () => ({
    type: 'REMOVE_START_DATE',
})

export const remove_endDate = () => ({
    type: 'REMOVE_END_DATE',
})
