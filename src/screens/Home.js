import React, {useState} from 'react'
import { View, Text, TextInput, StyleSheet, Dimensions, Image, Button } from 'react-native'
import News from "../components/News";
import Ionicons from 'react-native-vector-icons/dist/Ionicons';
import { useSelector, useDispatch } from 'react-redux';
import { query_search } from './../services/redux/action/search';
import Config from "react-native-config";

const {width, height} = Dimensions.get('window');

const Home = ({navigation, route}) => {
    const [search, setsearch] = useState();
    const isAdvancedSearch = useSelector((state) => state.isAdvancedSearch);
    const advSearchQueries = useSelector((state) => state.searchQueries);
    console.log('advanced = '+isAdvancedSearch)
    const dispatch = useDispatch();
    const searchHandler = () => dispatch(query_search());
    // const adv_search = route.params.advSearch?route.params.advSearch : 'Test'
    return (
        <View style={styles.container}>
            <View style={styles.carouselContainer}>
                <View style={styles.panelHeader}>
                    <View style={styles.searchBar}>
                        <Ionicons name="search" size={23} color="black" style={{padding: 3}} />
                        <TextInput style={styles.textInput} placeholder="Search"
                         onChangeText={(val)=>{
                          setsearch(val)
                         }} 
                          onFocus={()=>searchHandler()}
                          returnKeyType='search'
                         />
                    </View>
                </View>
            </View>
            <View style={styles.contentContainer}>
              {/* {isAdvancedSearch && <News navigation={navigation} search={route.params.advSearch} />  } */}
              {/* {!isAdvancedSearch && <News navigation={navigation} search={search} />  } */}
              <News navigation={navigation} search={isAdvancedSearch?advSearchQueries:search} />
              
            </View>
        </View>
    )
}

export default Home;


const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'space-evenly',
      alignItems: 'center',
    },
    carouselContainer: {
        flex: 1,
        width: width,
        backgroundColor: '#003366',
        paddingVertical: 15,
        paddingHorizontal: 20,
    },
    panelHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 10,
        alignItems: 'center',
    },
    contentContainer: {
        flex: 5,
        paddingTop: 30,
        paddingBottom: 5,
        paddingHorizontal: 10,
        width: width,
        backgroundColor: '#FFFFFF',
        borderTopStartRadius: 20,
        borderTopEndRadius: 20,
        marginTop: -30,
        elevation: 1,
    },
    searchBar: {
        flex: 3,
        flexDirection: 'row',
        paddingHorizontal: 20,
        borderColor: 'black',
        borderWidth: 1,
        backgroundColor: '#fff',
        borderRadius: 10,
        width: width*0.65,
        height: height*0.05,
    },
    textInput: {
        flex: 1,
        marginHorizontal: 10,
        color: 'black',
    },
    topContainer: {
      flex: 11,
      alignItems: 'center',
      justifyContent: 'center',
    },    
    bottomContainer: {
      flex: 1,
      justifyContent: 'flex-end',
      alignItems: 'center',
      marginBottom: 50,
    },   
    imageBackground: {
      flex: 1,
      resizeMode: "cover",
      alignItems: 'center'
    },
    label: {
      color: '#3EC6FF',
      fontSize: 16,
      fontWeight: 'bold',
      textAlign: 'right',
      alignSelf: 'flex-end',
    },
    title: {
      color: '#003366',
      fontSize: 20,
      fontWeight: 'bold',
      textAlign: 'center',
      marginBottom: 30,
    },
    text: {
      color: '#3EC6FF',
      fontSize: 16,
      fontWeight: 'bold',
      textAlign: 'center',
    },
    
  headerText: {
    fontSize: 18,
    fontWeight: 'bold',
  },

  //? Lanjutkan styling di sini
  itemContainer: {
    width: width * 0.44,
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#FFFFFF',
    marginHorizontal: 5,
    marginVertical: 5,
    padding: 5,
  },
  itemImage: {
    width: '90%',
    height: 100,
  },
  itemName: {
    textAlign: 'left',
    fontWeight: 'bold',
  },
  itemPrice: {
    color: 'blue',
    padding: 10,
  },
  itemStock: {
    fontWeight: 'bold',
  },
  itemButton: {
    padding: 5,
  },
  buttonText: {
    padding: 5,
  },
  });
  