import React, {useState} from 'react'
import {
    StyleSheet,
    Text,
    View,
    Dimensions
  } from "react-native";
import {Picker} from '@react-native-community/picker';
import Ionicons from 'react-native-vector-icons/dist/Ionicons';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import { RegisterButton } from '../components/Button';
import { adv_search, set_startDate, remove_startDate, set_endDate, remove_endDate } from './../services/redux/action/search';
import DateTimePicker from '@react-native-community/datetimepicker';

import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';

const {width, height} = Dimensions.get('window');

const Search = ({navigation}) => {
    const [date, setDate] = useState(new Date());

    const isSetStartDate = useSelector((state) => state.isSetStartDate);
    const isSetEndDate = useSelector((state)=>state.isSetEndDate);
    const selectedStartDate = useSelector((state)=>state.selectedStartDate);
    const selectedEndDate = useSelector((state)=>state.selectedEndDate);

    const [showStartDate, setShowStartDate] = useState(false);
    const [showEndDate, setShowEndDate] = useState(false);
    
    const [selectedLanguage, setSelectedLanguage] = useState("id");
    const [selectedSortBy, setSelectedSortBy] = useState("publishedAt");

    const dispatch = useDispatch();
    const searchHandler = (queries) => dispatch(adv_search(queries));
    const setStartDateHandler = (date) => dispatch(set_startDate(date));
    const setEndDateHandler = (date) => dispatch(set_endDate(date));
    const removeStartDateHandler = () => dispatch(remove_startDate());
    const removeEndDateHandler = () => dispatch(remove_endDate());
    const [searchQueries, setSearchQueries] = useState({q: 'a'});
    console.log(selectedEndDate);

    return (    
      <View style={styles.container}>
            <Text style={styles.title}>Search News</Text>
              
            <View style={styles.inputBar}>
                <TextInput style={styles.textInput} placeholder="Title" onChangeText={(val)=>{
                    setSearchQueries({...searchQueries, q: val}) 
                console.log(searchQueries) }
                 } />
            </View>
            <View style={styles.inputBar}>
                <TextInput style={styles.textInput} placeholder="Domains" onChangeText={(val)=>setSearchQueries({...searchQueries, domains: val})} />
            </View>

            <View style={styles.inputBar}>
                <Picker
                    style={styles.selectOption} 
                    prompt="Language"
                    selectedValue={selectedLanguage}
                    onValueChange={(lang) => {
                            setSelectedLanguage(lang)
                            setSearchQueries({...searchQueries, language: lang })
                        }
                    }
                >
                    <Picker.Item label="Argentina" value="ar" fontSize="1"/>
                    <Picker.Item label="Deutsch" value="de" />
                    <Picker.Item label="English" value="en" />
                    <Picker.Item label="Espanol" value="es" />
                    <Picker.Item label="French" value="fr" />
                    <Picker.Item label="Hebrew" value="he" />
                    <Picker.Item label="Bahasa Indonesia" value="id" />
                    <Picker.Item label="Italian" value="it" />
                    <Picker.Item label="Dutch" value="nl" />
                    <Picker.Item label="Norway" value="no" />
                    <Picker.Item label="Portugal" value="pt" />
                    <Picker.Item label="Russian" value="ru" />
                    <Picker.Item label="Sami" value="se" />
                    <Picker.Item label="Chinesse" value="zh" />
                </Picker>
            </View>

            <View style={styles.inputBar}>            
                <Picker
                    prompt="Sort By"
                    selectedValue={selectedSortBy}
                    style={styles.selectOption} 
    itemStyle={{ backgroundColor: "grey", color: "blue", fontFamily:"Ebrima", fontSize:1 }}
                    onValueChange={(sort) => {
                        setSelectedSortBy(sort)
                        setSearchQueries({...searchQueries, sortBy: sort })
                        }
                    } 
                >
                    <Picker.Item label="Sort by Relevancy" value="relevancy" />
                    <Picker.Item label="Sort by Popularity" value="popularity" />
                    <Picker.Item label="Sort by Date" value="publishedAt" />
                </Picker>

            </View>
              
            <TouchableOpacity onPress={()=>setShowStartDate(true)}>
                <View style={styles.inputBar}>
                    <Text style={{marginHorizontal: 14, marginTop: 6}}>{isSetStartDate?selectedStartDate:'Start Date'}</Text> 
                </View>
            </TouchableOpacity>

            <TouchableOpacity onPress={()=>setShowEndDate(true)}>
                <View style={styles.inputBar}>
                    <Text style={{marginHorizontal: 14, marginTop: 6}}>{isSetEndDate?selectedEndDate:'End Date'}</Text>                
                </View>
            </TouchableOpacity>
            

            <View>
            {showStartDate && (
                <DateTimePicker
                testID="startDate"
                value={date}
                mode="date"
                is24Hour={true}
                display="default"
                onChange={(event, selectedDate)=>{
                        setShowStartDate(false);
                        setSearchQueries({...searchQueries, from: moment(selectedDate).format().toString().substring(0, 10)})
                        setStartDateHandler(moment(JSON.stringify(selectedDate).substring(1, 11)).format('L'))
                        }
                    }
                />
            )}
            {showEndDate && (
                <DateTimePicker
                testID="endDate"
                value={date}
                mode="date"
                is24Hour={true}
                display="default"
                onChange={(event, selectedDate) => {
                        setShowEndDate(false);
                        const time = moment(selectedDate).format().toString().substring(0, 10);
                        setSearchQueries({...searchQueries, to: time})
                        setEndDateHandler(moment(JSON.stringify(selectedDate).substring(1, 11)).format('L'))
                        }
                    }
                />
            )}
            </View>
          
          <RegisterButton title="Search" style={{marginTop: width*0.05,}} onPress={()=>{
              searchHandler(searchQueries);
              removeStartDateHandler()
              removeEndDateHandler()
              navigation.navigate('Home', {screens: 'Home'})
            }
          }/>
    </View>
    );
};

const styles = StyleSheet.create({
    inputBar: {
        flexDirection: 'row',
        marginVertical: 10,
        borderColor: 'black',
        borderWidth: 1,
        backgroundColor: '#fff',
        borderRadius: width*0.01,
        width: width*0.75,
        height: height*0.05,
    },
    textInput: {
        flex: 1,
        marginHorizontal: 10,
        color: 'black',
    },
    selectOption: {
        flex: 1,
        marginTop: -width*0.02,
        marginHorizontal: 5,
        color: 'black',
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: 1,
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        paddingVertical: 20,
        paddingHorizontal: 30,
        margin: width * 0.03,
        borderRadius: width * 0.05,
        shadowColor: '#000',
        shadowOffset: { width:0.5, height: 0.5 },
        shadowOpacity: 0.5,
        shadowRadius: 3,
        elevation: 3,
        zIndex: 1,
    },
    title: {
        marginHorizontal: width * 0.05,
        marginVertical: width * 0.03,
        color: 'black',
        fontSize: 20,
        fontWeight: 'bold'
  
    },
    description: {
        marginVertical: width * 0.05,
        marginHorizontal: width * 0.02,
        color: 'gray',
        fontSize: 18
    },
    image: {
        height: height / 6,
        marginLeft: width * 0.05,
        marginRight: width * 0.05,
        marginVertical: height * 0.02
    },
    author: {
        marginBottom: width * 0.01,
        marginHorizontal: width * 0.05,
        fontSize: 15,
        color: 'gray'
  
    },
    date: {
      marginBottom: width * 0.01,
      marginHorizontal: width * 0.05,
      fontSize: 10,
      color: 'gray'
    },
  })

export default Search;
