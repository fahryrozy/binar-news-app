import React from 'react'
import { View, Text } from 'react-native'
import { WebView } from 'react-native-webview';

const Detail = ({navigation, route}) => {
    return (
        <WebView source={{ uri: route.params.url }} />
    )
}

export default Detail
