import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
} from 'react-native';

export const RegisterButton = ({style, title, onPress}) => {
  return (
    <TouchableOpacity style={[styles.registerButton, style]} onPress={onPress}>
      <View>
        <Text style={styles.textButton}>{title}</Text>
      </View>
    </TouchableOpacity>
  )
}

export const LoginButton = ({style, title, onPress}) => {
  return (
    <TouchableOpacity style={[styles.loginButton, style]} onPress={onPress}>
      <View>
        <Text style={styles.textButton}>{title}</Text>
      </View>
    </TouchableOpacity>
  )
}
export const LinkButton = ({style, title, onPress}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <Text style={[styles.link, style]}>{title}</Text>
    </TouchableOpacity>
  )
}

export const AddButton  = ({style, title, onPress}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <Text style={[styles.addButton, style]}>Add</Text>
    </TouchableOpacity>
  )
}

export const RemoveButton  = ({style, title, onPress}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <Text style={[styles.removeButton, style]}>Remove</Text>
    </TouchableOpacity>
  )
}

export const GoBack = ({onPress}) => (
  <TouchableOpacity onPress={onPress} >
    <View style={{backgroundColor: '#003366', width: 100, paddingHorizontal: 10, paddingVertical: 5, marginVertical: 20,}} >
        <Text style={{color: '#FFF', textAlign: 'center'}}>Go Back</Text>
    </View> 
  </TouchableOpacity>
)


export const Logout = ({onPress}) => (
  <TouchableOpacity onPress={onPress} >
    <View style={{backgroundColor: '#d01c00', width: 100, paddingHorizontal: 10, paddingVertical: 5, marginVertical: 20,}} >
        <Text style={{color: '#FFF', textAlign: 'center'}}>Logout</Text>
    </View> 
  </TouchableOpacity>
)
  
  
const styles = StyleSheet.create({
    textButton: {
      color: 'white',
      textAlign: 'center',
      padding: 5,
      fontSize: 15,
    },
    loginButton: {
      width: 200,
      backgroundColor: '#003366',
      paddingHorizontal: 10,
      paddingVertical: 5,
      borderRadius: 10,
      margin: 5,
      textAlign: 'center',
    },  
    registerButton: {
      width: 200,
      backgroundColor: '#003366',
      paddingHorizontal: 10,
      paddingVertical: 5,
      borderRadius: 10,
      margin: 5,
      textAlign: 'center',
    },        
    link: {
      color: '#3EC6FF',
      fontSize: 13,
      fontWeight: 'bold',
      textAlign: 'center',
    },
    addButton: {
      backgroundColor: '#003366',
      color: '#FFFFFF',
      borderRadius: 10,
      borderWidth: 1,
      paddingHorizontal: 10,
      paddingVertical: 5,
      width: 100,
      textAlign: 'center',
      fontSize: 12,
    },
    removeButton: {
      borderColor: '#ff3102',
      backgroundColor: '#ff3102',
      color: '#FFFFFF',
      borderRadius: 10,
      borderWidth: 1,
      paddingHorizontal: 10,
      paddingVertical: 5,
      width: 100,
      textAlign: 'center',
      fontSize: 12,
    },
  });
  