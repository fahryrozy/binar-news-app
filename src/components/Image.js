import React from 'react';
import {
  StyleSheet,
  Image,
  Dimensions,
} from 'react-native';

const { width, height } = Dimensions.get("window");

export const SplashLogo = () => {
  const logo = require('./../assets/image/world.png')
  return (
      <Image style={styles.welcomelogo} source={logo} />
  )
}

const styles = StyleSheet.create({
    logo: {
        marginTop: 25,
        padding: 10,
        justifyContent: 'center',
        alignContent: 'center',
    },
    welcomelogo: {
      marginTop: 25,
      padding: 10,
      justifyContent: 'center',
      alignContent: 'center',
      width: width*0.6,
      height: height*0.15,
  },
    imageBackground: {
      flex: 1,
      resizeMode: 'cover',
      justifyContent: 'center',
      position: 'absolute',
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
    },
    unauthorized: {
      resizeMode: 'contain',
      justifyContent: 'center',
      position: 'absolute',
    }
});
  