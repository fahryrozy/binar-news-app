import React, {useState, useEffect} from 'react'
import { View, Text, Image, StyleSheet, ActivityIndicator, Dimensions } from 'react-native'
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import api from '../services/api';
import moment from "moment";
import Config from "react-native-config";

const { width, height } = Dimensions.get('window')

const NewsCardLists = ({item, navigation}) => {
  const time = moment(item.publishedAt, "YYYY-MM-DD h:mm:ss a").calendar()
  return (
      <View style={styles.cardView}>
        <TouchableOpacity onPress={()=>navigation.navigate('Detail', {url: item.url})}>
          <Text style={styles.title}>{item.title}</Text>
          <Text style={styles.author}>Ditulis oleh : {item.author} </Text>
          <Text style={styles.date}>{time} </Text>
                      
          <Image style={styles.image} source={item.urlToImage ? {uri: item.urlToImage } : null}/>
          <Text style={styles.description}>{item.description}</Text>
        </TouchableOpacity>
      </View>
  )
}
const News = ({search, navigation}) => {
  const [news, setNews] = useState([]);
  const [isEndOfList, setisEndOfList] = useState(false);
  const [offset, setOffset] = useState(1);
  
  useEffect(() => {
    setOffset(offset+1)
  }, [isEndOfList]);
  
  useEffect(() => {
    getNews();
  }, [search, isEndOfList]);
  
  const getNews = () => {
    if(search) {
      if(typeof(search)==='string') {
        console.log(typeof search)
        api.get(`everything?q=${search}&page=${offset}&apiKey=${Config.API_KEY}`)
            .then(async function (response) {
                setNews(response.data.articles)
            })
            .catch(function (error) {
                console.log(error)
            })
      }
      else {
        console.log('Unmapped = '+JSON.stringify(search))
        const keyArr = Object.keys(search);
        const valArr = Object.values(search);
        const queries = keyArr.map((item, index)=>item+'='+valArr[index]+'&').toString().replace(/,/g,"");
        console.log('queris = '+queries)
        api.get(`everything?${queries}page=${offset}&apiKey=${Config.API_KEY}`)
            .then(async function (response) {
                setNews(response.data.articles);
            })
            .catch(function (error) {
                console.log(error)
            })

      }
    }
    else {
      api.get(`top-headlines?country=id&page=${offset}&apiKey=${Config.API_KEY}`)
          .then(async function (response) {
            if(!isEndOfList) {
              setNews(response.data.articles);
            }
            else {
              setNews([...news, ...response.data.articles])
            }
          })
          .catch(function (error) {
              console.log(error)
          })
    }
  }

  return (
    <View>
        <FlatList data={news}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item}) => <NewsCardLists item={item} navigation={navigation} /> }
            onEndReached={()=>{
              setisEndOfList(true)
              console.log(news.length)
              console.log(offset)
              }
            }
            onEndReachedThreshold={0.5}

        />
    </View>
  )
}

export default News;

const styles = StyleSheet.create({
  cardView: {
      backgroundColor: 'white',
      margin: width * 0.03,
      borderRadius: width * 0.01,
      shadowColor: '#000',
      shadowOffset: { width:0.5, height: 0.5 },
      shadowOpacity: 0.3,
      shadowRadius: 3,
      elevation: 5,
      zIndex: 1,
  },
  title: {
      marginHorizontal: width * 0.05,
      marginVertical: width * 0.03,
      color: 'black',
      fontSize: 20,
      fontWeight: 'bold'

  },
  description: {
      marginVertical: width * 0.05,
      marginHorizontal: width * 0.05,
      color: 'gray',
      fontSize: 16,
  },
  image: {
      height: height / 6,
      marginLeft: width * 0.05,
      marginRight: width * 0.05,
      marginVertical: height * 0.02
  },
  author: {
      marginBottom: width * 0.01,
      marginHorizontal: width * 0.05,
      fontSize: 13,
      color: '#003366'

  },
  date: {
    marginBottom: width * 0.01,
    marginHorizontal: width * 0.05,
    fontSize: 10,
    color: 'gray'
  },
})